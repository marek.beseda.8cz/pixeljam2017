﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour {
	public HexCell HexCell;
	public float StepTime = 10;
	protected float StepTimer;
	
	void Awake () {
		HexCell = GetComponent<HexCell>();
		StepTimer = StepTime;
	}
	
	void Update () {
		StepTimer -= Time.deltaTime;
		if (StepTimer < 0) {
			StepTimer = StepTime;
			Step();
		}
	}

	void Step() {
		
	}
}
