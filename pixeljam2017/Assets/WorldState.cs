﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldState : MonoBehaviour
{
	// SINGLETON INITIALISATION
	private static WorldState instance;
	public int MoneyAmount;
	private const int InitialMoney = 1000000;

	public WorldState get()
	{
		return instance;
	}

	private void Awake()
	{
		instance = new WorldState();
	}

	
	// State
	private int _Money;

	public int Money
	{
		get { return _Money; }
		set { _Money = value; }
	}
}
